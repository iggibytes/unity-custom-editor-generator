using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TestComponent))]
public class TestComponentEditor : Editor
{

	SerializedProperty spPublicFloat;
	SerializedProperty spPrivateSerializedFloat;
	SerializedProperty spRangeFloat;
	SerializedProperty spTestData;

	void OnEnable()
	{
		spPublicFloat = serializedObject.FindProperty("publicFloat");
		spPrivateSerializedFloat = serializedObject.FindProperty("privateSerializedFloat");
		spRangeFloat = serializedObject.FindProperty("rangeFloat");
		spTestData = serializedObject.FindProperty("testData");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		EditorGUILayout.Space();

		EditorGUILayout.PropertyField (spPublicFloat, true);

		EditorGUILayout.PropertyField (spPrivateSerializedFloat, true);

		EditorGUILayout.PropertyField (spRangeFloat, true);

		EditorGUILayout.PropertyField (spTestData, true);


		serializedObject.ApplyModifiedProperties();
	}

	/*
	public void OnSceneGUI()
	{
	}
	*/

}
